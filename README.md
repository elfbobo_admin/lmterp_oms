### 参与贡献
- 龙里
- 微信号: isa1589518286
- 体验地址: http://demo-erp.lmterp.cn
- 账号：manager
- 密码：123456

### 独立商城
- 国内：[http://demo1.lmterp.cn](http://demo1.lmterp.cn)
- 跨境：[http://demo2.lmterp.cn](http://demo2.lmterp.cn)

### 默认已经配置好 php 环境并且安装好 swoole
- 推荐使用 [LNMP](https://lnmp.org/install.html) 一键安装包
- 环境依赖版本

  1）Centos7 建议
  
  2）php7.2 及以上
  
  3）Mysql5.7 及以上
- 数据文件导入  `项目根目录/data/lmterp.sql`
- nginx 配置文件如下
```
  server {
      listen       80;
      server_name 你的域名;
      root 项目路径/public;
      access_log  /var/log/nginx/access.log;
      error_log  /var/log/nginx/error.log;
      location / {
                if (!-e $request_filename) {
            rewrite  ^(.*)$  /index.php?s=/$1  last;
         }
        index index.php index.html index.htm;
      }
            location ~ \.php$ {
          fastcgi_pass   unix:/tmp/php-cgi.sock;
          fastcgi_index  index.php;
          fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
          include       /usr/local/nginx/conf/fastcgi_params;
      }
  }
```
- 项目使用TP5.1开发，[TP5.1使用手册](https://www.kancloud.cn/manual/thinkphp5_1/353948)
- 切换到项目根路径执行如下命令
  1）更新依赖包 `composer update`  
  2）启动swoole  `php think swoole start`

### 项目架构
```
|----application 应用程序
| |----swoole  swoole 模块
| | |----timer  定时器
| | |----controller  控制器
| |----admin  后台模块
| | |----validate  验证
| | |----controller  控制器
| | | |----order  订单模块
| | | |----cha  物流模块
| | | |----wms  仓库模块
| | | |----auth  权限模块
| | | |----pur  采购模块
| | | |----system  系统模块
| | | |----cus  客服模块
| | |----report  报表模块
| | | |----cra  采集模块
| | | |----pub  刊登模块
| | |----view  视频模板
| |----common  公共模块
| | |----library  工具类
| | |----status  状态
| | |----model  模型层
| | |----service  服务层
| | | |----platform  平台
| | | |----product   商品
| | | |----channel   渠道
| | | |----system    系统
| | | |----purchase  采购
| | | |----orders   订单
| | | |----export   导出
| | | |----import   导入
| | | |----logistics  物流
| |----command  控制台命令Console
|----config  配置文件
|----extend  第三方类库
|----route  路由
|----public  项目目录
```

### 已开发模块
#### 首页

#### 产品库
- 分类管理（只支持三级分类）
- 产品管理
- 产品搜索
- 产品导入导出
- 平台SKU管理
- 平台SKU导入导出

#### 订单管理
- 订单管理
- 渠道选择
- 打印面单&批量打印
- 订单搜索
- 订单手动导入导出
- 已对接如下平台实现自动抓单
```
  1. Allegro
  2. Amazon
  3. Blibli
  4. Cdiscout
  5. Coupang
  6. Eprice
  7. Joom
  8. Lazada
  9. Newegg
  10. Rakuten 德国
  11. Real
  12. Shopee
```
#### 物流
- 承运商管理
- 渠道管理
- 申报订单管理
- 已对接渠道
```
1. 华翰易速通
```
#### 系统管理
- 平台管理
- 店铺管理
- 国家管理
- 汇率管理
- 配置管理
- 系统设置
- 权限管理
- 回收站（所有删除的表数据都会经过回收站）

### 开发中模块
- 采购
- 仓库

### 规划模块
- 财务
- 报表
- 客服
- 采集
- 刊登

### 项目更新日志
- 2020-10-09
```
1. 添加系统说明文档
2. 修改已申报订单展示样式
```
