<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/02
 * Time: 15:20
 * @link http://www.lmterp.cn
 */

namespace app\command;

use app\common\library\Tools;
use app\common\model\Account;
use app\common\model\AccountSettings;
use app\common\service\platform\BasePlatformService;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Log;

/**
 * 获取平台订单
 * Class GetOrder
 * @package app\command
 */
class GetOrder extends Command
{
    /**
     * @var Account
     */
    protected $account;

    protected $params = [];

    protected function configure()
    {
        $this->setName('getOrder')
            ->addArgument("account_id", Argument::REQUIRED, "账号ID")
            ->addOption("start_date", null, Option::VALUE_REQUIRED, "开始时间")
            ->addOption("end_date", null, Option::VALUE_REQUIRED, "结束时间")
            ->addOption("status", null, Option::VALUE_REQUIRED, "订单状态")
            ->addOption("page_size", null, Option::VALUE_REQUIRED, "分页大小")
            ->addOption("json", null, Option::VALUE_REQUIRED, "json参数，json格式")
            ->setDescription("抓单");
    }

    protected function execute(Input $input, Output $output)
    {
        // 设置参数
    	$this->setParams();
    	$accountId = $this->input->getArgument("account_id");
    	// 开始处理抓单逻辑
        if(!($this->account = Account::where(["account_id" => $accountId, "is_active" => Account::ACTIVE_Y])->find()))
        {
            $output->write(0);
            Log::info(sprintf("账号【%d】不存在, 或状态已关闭", $accountId));
            return false;
        }
        if(!($classes = $this->account->platform->classes))
        {
            $output->write(0);
            Log::info(sprintf("账号【%d】平台【%s】对接类未配置", $accountId, $this->account->platform->name));
            return false;
        }
        $classes = BasePlatformService::getClasses($classes);
        if(!BasePlatformService::classesExist($classes))
        {
            $output->write(0);
            Log::info(sprintf("账号【%d】平台【%s】对接类【%s】不存在", $accountId, $this->account->platform->name, $classes));
            return false;
        }
        /**
         * @var BasePlatformService $obj
         */
        $obj = new $classes($accountId);
        $obj->getOrderList($this->params);
        $obj->syncOrderByAccount($accountId);
        // 更新状态
        $settings = $this->account->settings;
        $settings->finish_sync_time = Tools::now();
        $settings->status = AccountSettings::STATUS_SUC;
        $settings->save();
        $output->write(1);
    }

    /**
     * 设置参数
     * @date 2020/09/19
     * @author longli
     */
    private function setParams()
    {
        $args = ['start_date', 'end_date', 'status', 'page_size'];
        foreach($args as $v)
            if($this->input->hasOption($v)) $this->params[$v] = $this->input->getOption($v);
        if($this->input->hasOption("json") && Tools::isJson($this->input->getOption("json"), $json))
            $this->params += $json;
    }
}
