<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/27
 * Time: 22:34
 * @link http://www.lmterp.cn
 */

namespace app\command;

use app\common\model\Orders;
use app\common\model\ReportOrderDay;
use app\common\service\orders\OrderService;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;

/**
 * 统计订单
 * Class StatOrder
 * @package app\command
 */
class StatOrder extends Command
{
    protected function configure()
    {
        $this->setName('statorder')
            ->addOption("start_date", null, Option::VALUE_REQUIRED, "开始统计时间，默认为今天");
    }

    protected function execute(Input $input, Output $output)
    {
        $this->statByAccount();
    }

    /**
     * 按账号统计订单
     * @date 2020/09/27
     * @author longli
     */
    private function statByAccount()
    {
        $startDate = $this->input->hasOption("start_date")
                    ? $this->input->getOption("start_date")
                    : date('Y-m-d');
        $orders = Orders::field(["COUNT(account_id) total_order", "SUM(total_price_rmb) total_price", "account_id", "'$startDate' stat_date"])
            ->where([
                ["order_source_create_time", ">=", $startDate]
            ])->group(["account_id"])
            ->select();
        foreach($orders as $order)
        {
            OrderService::getInstance()->addStatOrder($order->toArray());
        }
    }
}
