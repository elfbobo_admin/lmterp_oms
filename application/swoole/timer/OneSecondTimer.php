<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/19
 * Time: 20:57
 * @link http://www.lmterp.cn
 */

namespace app\swoole\timer;

use app\common\library\Tools;
use app\common\model\AccountSettings;
use app\common\service\logistics\ChannelService;
use app\common\service\orders\OrderService;
use app\common\service\system\AccountService;
use think\facade\Log;

/**
 * 每秒执行入口
 * Class OneSecond
 * @package app\swoole\timer
 */
class OneSecondTimer extends BaseTimer
{
    public function run()
    {
        //$this->getOrder();
        $this->declareOrder();
    }

    /**
     * 抓单
     * @date 2020/09/20
     * @author longli
     */
    private function getOrder()
    {
        $accountService = AccountService::getInstance();
        // 处理抓单
        foreach(AccountSettings::with(['account'])->where([
            ['is_auto_sync', '=', AccountSettings::AUTO_SYNC_Y],
            ['status', 'IN', [AccountSettings::STATUS_NONE, AccountSettings::STATUS_SUC]],
        ])->select() as $settings)
        {
            echo "抓取订单\n";
            $last = strtotime($settings->last_sync_time);
            $hours = $settings->hours * 3600;
            if(!empty($settings->last_sync_time) && time() - $last < $hours) continue;
            $accountService->syncOrderByAccount($settings->account_id);
        }

        // 处理失败自动重置
        $accountService->resetSyncStatus();
    }

    /**
     * 申报订单获取追踪号和面单
     * @date 2020/09/25
     * @author longli
     */
    private function declareOrder()
    {
        $service = ChannelService::getInstance();
        foreach(OrderService::getInstance()->getDeclareOrder() as $order)
        {
            $service->declareOrder($order->channel_id, $order);
        }
    }
}