<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/19
 * Time: 20:57
 * @link http://www.lmterp.cn
 */

namespace app\swoole\timer;

/**
 * 每小时执行入口
 * Class OneSecond
 * @package app\swoole\timer
 */
class OneHourTimer extends BaseTimer
{
    public function run()
    {
        echo "\t\t\t每小时执行  " . date('Y-m-d H:i:s') . "\n";
    }
}