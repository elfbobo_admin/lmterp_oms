<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/19
 * Time: 21:00
 * @link http://www.lmterp.cn
 */

namespace app\swoole\timer;

/**
 * 十分钟执行入口
 * Class OneMinute
 * @package app\swoole\timer
 */
class TenMinuteTimer extends BaseTimer
{
    public function run()
    {
        echo "\t\t每十钟执行  " . date('Y-m-d H:i:s') . "\n";
    }
}