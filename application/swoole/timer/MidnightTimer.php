<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/19
 * Time: 21:03
 * @link http://www.lmterp.cn
 */

namespace app\swoole\timer;

/**
 * 每天凌晨执行入口
 * Class MidnightTimer
 * @package app\swoole\timer
 */
class MidnightTimer extends BaseTimer
{
    public function run()
    {
        echo "\t\t\t\t凌晨12点执行  " . date('Y-m-d H:i:s') . "\n";
    }
}