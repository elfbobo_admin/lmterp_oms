<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/15
 * Time: 09:57
 * @link http://www.lmterp.cn
 */

namespace app\swoole\controller;


use app\common\library\Config;
use think\Controller;

abstract class BaseController extends Controller
{
}