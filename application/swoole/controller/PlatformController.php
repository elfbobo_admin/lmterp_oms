<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/14
 * Time: 19:46
 * @link http://www.lmterp.cn
 */

namespace app\swoole\controller;

use app\common\model\AccountSettings;
use think\Console;
use think\swoole\facade\Task;

/**
 * 平台控制器
 * Class PlatformController
 * @package app\swoole\controller
 */
class PlatformController extends BaseController
{
    public function getOrder()
    {
        $accountId = $this->request->get('account_id');
        if(empty($accountId)) return "非法请求";
        $settings = AccountSettings::get(['account_id' => $accountId]);
        $params = [$accountId];
        if($settings)
        {
            $startDate = date('Y-m-d H', strtotime("-{$settings->auto_sync_hours}hours"));
            $params[] = "--start_date={$startDate}:00:00";
        }
        $out = Console::call('getOrder', $params)->fetch();
        if(!$out && $settings)
        {
            $settings->status = AccountSettings::STATUS_ERR;
            $settings->error_count += 1;
            $settings->save();
        }
        return "抓单";
    }
}