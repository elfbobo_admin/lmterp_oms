<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/6/10
 * Time: 14:28
 * @link http://www.lmterp.cn
 */

namespace app\admin\validate;


use think\Validate;

class Article extends Validate
{
    protected $rule = [
        'title|标题'  => 'require|max:64',
    ];

}