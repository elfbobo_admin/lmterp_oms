<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/04
 * Time: 17:14
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\cha;


use app\admin\controller\BaseController;
use app\common\library\Tools;
use app\common\model\Channel;
use app\common\model\ChannelCarrier;
use app\common\model\ChannelOrders;
use app\common\service\channel\BaseChannel;
use app\common\service\logistics\ChannelService;
use app\common\status\BaseStatus;
use think\facade\Validate;

/**
 * 渠道管理
 * Class IndexController
 * @package app\admin\controller\channel
 */
class ChannelController extends BaseController
{
    /**
     * 渠道列表
     * @date 2020/09/18
     * @author longli
     */
    public function index()
    {
        if(!$this->request->isAjax()) return $this->fetch('index');
        $this->assign('list', Channel::with(['carrier'])->order("sort desc,channel_name")->select());
        return $this->fetch('lists');
    }

    /**
     * 更新渠道
     * @date 2020/09/18
     * @author longli
     */
    public function update()
    {
        if($this->request->isPost())
        {
            $data = $this->request->post();
            // 普通验证
            $validate = Validate::make([
                'carrier_id'  => 'require',
                'channel_name'  => 'require',
                'channel_type'  => 'require',
                'day_max_shipping'  => 'egt:0',
                'default_weight'  => 'egt:0',
                'default_discount'  => 'egt:0',
                'label_width'  => 'egt:0',
                'label_height'  => 'egt:0',
                'classes' => function($value, $rule)
                {
                    if(empty($value)) return true;
                    if(!BaseChannel::classesExist($value))return "对接类【{$value}】不存在";
                    return true;
                }

            ],[
                'carrier_id.require' => '渠道必选',
                'channel_name.require' => '渠道名称必填',
                'channel_type.require' => '渠道类型必选',
                'day_max_shipping.egt' => '单日发货量有误',
                'default_weight.egt' => '起重有误',
                'default_discount.egt' => '拆扣有误',
                'label_width.egt' => '面单宽度有误',
                'label_height.egt' => '面单高度有误',
            ]);
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));

            if(!empty($data['channel_id']))
            {
                if(($token = $this->validateToken($data)) && !is_array($token)) $this->error($token);
                $data['token'] = $token;
            }
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            if(!isset($data['status'])) $data['status'] = Channel::STATUS_N;
        }
        else
        {
            $validate = Validate::make([
                'channel_id'  => 'require',
            ],[
                'channel_id.require' => '非法请求',
            ]);
            $data = $this->request->get();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        ChannelService::getInstance()->addChannel($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 录入渠道
     * @date 2020/09/18
     * @author longli
     */
    public function add()
    {
        $this->assign("channel_type", Channel::$CHANNEL_TYPE);
        $this->assign("carrier", ChannelCarrier::getAll());
        $this->assign("is_track", Channel::$IS_TRACK);
        $this->assign("track_type", Channel::$TRACK_TYPE);
        $this->assign("label_type", Channel::$LABEL_TYPE);
        $this->assign("service", BaseChannel::getAllChannelService());
        $channelId = $this->request->request("channel_id");
        if(!empty($channelId))
        {
            $channel = Channel::get($channelId);
            $this->assign("channel", $channel);
            if(($classes = BaseChannel::getClasses($channel->classes)) && BaseChannel::classesExist($classes))
            {
                $this->assign("tokenField", $classes::$tokenField);
            }
        }
        return $this->fetch("add");
    }

    /**
     * 删除渠道
     * @date 2020/09/18
     * @author longli
     */
    public function delete()
    {
        $channelId = $this->request->request("ids");
        if(empty($channelId)) $this->error('参数错误');
        Channel::destroy($channelId);
        $this->success('删除成功');
    }

    /**
     * 获取渠道列表
     * @return array
     * @date 2020/09/18
     * @author longli
     */
    public function channelList()
    {
        $carrierId = $this->request->request("carrier_id");
        if(empty($carrierId)) return apiResponse(BaseStatus::CODE_FAULT, [], '渠道必传');
        $data = Channel::getByCarrierId($carrierId);
        return apiResponse(BaseStatus::CODE_NORMAL, $data, "获取成功");
    }

    /**
     * 验证渠道录入的 token 信息
     * @param array $post 表单提交过来的数据
     * @return string|array
     * @date 2020/09/17
     * @author longli
     */
    private function validateToken($post = [])
    {
        if(empty($post['classes'])) return [];
        $classes = BaseChannel::getClasses($post['classes']);
        if(!BaseChannel::classesExist($classes)) return "对接类不存在";
        $token = [];
        foreach($post as $k => $v)
        {
            if(Tools::startWith($k, 'token_') && !empty($v) && !is_numeric($v)) $token[substr($k, 6)] = $v;
        }

        $tokenField = $classes::$tokenField;
        $flag = false;
        foreach($tokenField['required'] as $r)
        {
            if(!empty($token[$r['field']]))
            {
                $flag = true;
                break;
            }
        }
        if($flag)
        {
            if(empty($post['api_base_url'])) return "【api请求根路径】能不为空";
            if(!Tools::startWith($post['api_base_url'], 'http')) return "【api请求根路径】不是正确的url";
            foreach($tokenField['required'] as $r)
            {
                if(empty($token[$r['field']])) return "【{$r['field']}】不能为空";
            }
        }
        return $flag ? $token : [];
    }
}