<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/18
 * Time: 15:56
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\cha;


use app\admin\controller\BaseController;
use app\common\model\Account;
use app\common\model\AccountPlatform;
use app\common\model\ChannelOrders;
use app\common\model\Orders;
use app\common\service\logistics\ChannelService;

class OrderController extends BaseController
{
    /**
     * 渠道订单列表
     * @date 2020/09/18
     * @author longli
     */
    public function index()
    {
        $this->assign("platform", AccountPlatform::getAll());
        if(!$this->request->isAjax()) return $this->fetch('index');
        $model = ChannelOrders::with(['order.account', 'channel'])->order("is_cancel, ch_id desc");
        // 处理搜索
        $this->search($model);
        $limit = ChannelService::getInstance()->getPageSize($this->request->request());
        $ret = $model->paginate($limit);
        $this->assign('list', $ret->getCollection());
        $this->assign('page', $ret->render());
        return $this->fetch('lists');
    }

    /**
     * 渠道订单搜索
     * @param ChannelOrders $model 渠道订单模型
     * @date 2020/09/18
     * @author longli
     */
    private function search($model)
    {
        $platform_id = $this->request->request("platform_id", '', 'trim');
        $name = $this->request->request("name", '', 'trim');
        $order_no = $this->request->request("order_no", '', 'trim');
        $track_number = $this->request->request("track_number", '', 'trim');
        $date = $this->request->request("date", '', 'trim');
        // 平台搜索
        if(!empty($platform_id))
        {
            $model->where("order_id", "in", function($query)use($platform_id)
            {
                $query->table(Orders::getTable() . " o")
                        ->join(Account::getTable() . " a", "a.account_id=o.account_id")
                        ->join(AccountPlatform::getTable(). " p", "a.platform_id=p.platform_id")
                        ->where("p.platform_id", $platform_id)
                        ->field("o.order_id");
            });
        }
        // 店铺或者账号搜索
        if(!empty($name))
        {
            $model->where("order_id", "in", function($query)use($name)
            {
                $query->table(Orders::getTable() . " o")
                    ->join(Account::getTable() . " a", "a.account_id=o.account_id")
                    ->whereRaw("a.username = :a or store_name = :b", ["a"=>$name, "b"=>$name])
                    ->field("order_id");
            });
        }
        // 订单号搜索
        if(!empty($order_no))
        {
            $model->where("order_id", "in", function($query)use($order_no)
            {
                $query->table(Orders::getTable() . " o")
                    ->where("order_no", $order_no)
                    ->field("order_id");
            });
        }
        // 运单号搜索
        if(!empty($track_number))
        {
            $model->where("track_number", $track_number);
        }
        // 申报时间搜索
        if(!empty($date))
        {
            $model->where(ChannelService::getInstance()->parseLayuiRangeDate('create_time', $date));
        }
    }

    /**
     * 取消申报订单
     * @date 2020/09/25
     * @author longli
     */
    public function cancel()
    {
        $chId = $this->request->get("ch_id");
        if(empty($chId)) $this->error('参数错误');
        $chOrder = ChannelOrders::get($chId);
        if($chOrder->is_cancel == ChannelOrders::CANCEL_Y) $this->error("订单是已取消状态");
        ChannelService::getInstance()->cancelOrder($chOrder->channel_id, $chOrder->order_id)
            ? $this->success("取消成功")
            : $this->error("取消失败");
    }

    /**
     * 删除渠道订单
     * @date 2020/09/18
     * @author longli
     */
    public function delete()
    {
        $orderId = $this->request->request("ids");
        if(empty($orderId)) $this->error('参数错误');
        ChannelOrders::destroy($orderId);
        $this->success('删除成功');
    }
}