<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/18
 * Time: 15:56
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\cha;


use app\admin\controller\BaseController;
use app\common\model\ChannelSender;
use app\common\service\logistics\ChannelService;
use think\facade\Validate;

class SenderController extends BaseController
{
    /**
     * 发件人列表
     * @date 2020/09/18
     * @author longli
     */
    public function index()
    {
        if(!$this->request->isAjax()) return $this->fetch('index');
        $this->assign('list', ChannelSender::order("is_default desc,sender_id desc")->select());
        return $this->fetch('lists');
    }

    /**
     * 更新发件人
     * @date 2020/09/18
     * @author longli
     */
    public function update()
    {
        if($this->request->isPost())
        {
            $validate = Validate::make([
                'name'  => 'require',
                'phone'  => 'require',
                'mobile'  => 'require',
                'country_code'  => 'require',
                'province'  => 'require',
                'city'  => 'require',
                'address'  => 'require',
            ],[
                'name.require' => '发件人名称必填',
                'phone.require' => '电话必填',
                'mobile.require' => '手机必填',
                'country_code.require' => '国家代码必填',
                'province.require' => '省份必填',
                'city.require' => '城市必填',
                'address.require' => '地址必填',
            ]);
            $data = $this->request->post();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            if(!isset($data['status'])) $data['status'] = ChannelSender::STATUS_N;
        }
        else
        {
            $validate = Validate::make([
                'sender_id'  => 'require',
            ],[
                'sender_id.require' => '非法请求',
            ]);
            $data = $this->request->get();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        ChannelService::getInstance()->addSender($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 录入发件人
     * @date 2020/09/18
     * @author longli
     */
    public function add()
    {
        $senderId = $this->request->request("sender_id");
        if(!empty($senderId)) $this->assign("sender", ChannelSender::get($senderId));
        return $this->fetch("add");
    }

    /**
     * 删除发件人
     * @date 2020/09/18
     * @author longli
     */
    public function delete()
    {
        $senderId = $this->request->request("ids");
        if(empty($senderId)) $this->error('参数错误');
        ChannelSender::destroy($senderId);
        $this->success('删除成功');
    }
}