<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/17
 * Time: 19:55
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\system;


use app\admin\controller\BaseController;
use app\common\model\Account;
use app\common\model\AccountPlatform;
use app\common\model\AccountSettings;
use app\common\service\system\AccountService;
use think\facade\Validate;

class SettingsController extends BaseController
{
    /**
     * 平台首页
     * @return string
     * @date 2020/09/17
     * @author longli
     */
    public function index()
    {
        if(!$this->request->isAjax())
        {
            $this->assign("platform", AccountPlatform::getAll());
            return $this->fetch('index');
        }
        $where = [];
        $sort = "sort desc, platform_id, username";
        $account = Account::with(['settings', 'platform'])->order($sort);
        // 解析查询条件
        if($pid = $this->request->request("platform_id"))
            $where[] = ['platform_id', "=", $pid];
        if($date = $this->request->request("date"))
            $where = array_merge($where, AccountService::getInstance()->parseLayuiRangeDate('create_time', $date));
        if($name = $this->request->request("name"))
            $account->whereRaw("username like :a or store_name like :b", ["a"=>"%$name%", "b"=>"%$name%"]);
        $limit = AccountService::getInstance()->getPageSize($this->request->request());
        // 执行分页查询
        $ret = $account->where($where)->paginate($limit);
        $this->assign("list", $ret->getCollection());
        $this->assign("page", $ret->render());
        return $this->fetch('lists');
    }


    /**
     * 保存
     * @date 2020/09/17
     * @author longli
     */
    public function save()
    {
        if(!$this->request->isPost()) $this->error('非法操作');
        $post = $this->request->post();
        $validate = Validate::make([
            'is_auto_sync'  => 'require',
            'auto_sync_hours' => 'integer|between:1,99',
            'hours' => 'integer|between:1,99'
        ],[
            'is_auto_sync.require' => '同步方式必选',
            'auto_sync_hours.integer' => '同步时间必须是整数',
            'auto_sync_hours.between' => '时间范围只能在[1-99]',
            'hours.integer' => '小时必须是整数',
            'hours.between' => '小时范围只能在[1-99]',
        ]);
        if(!$validate->batch()->check($post))$this->error(join(', ', $validate->getError()));
        AccountService::getInstance()->addAccountSettings($post)
            ? $this->success('保存成功')
            : $this->error('保存失败');
    }

    /**
     * 同步订单
     * @date 2020/09/21
     * @author longli
     */
    public function syncOrder()
    {
        $accountId = $this->request->get('account_id');
        if(empty($accountId)) $this->error('非法操作');
        try
        {
            AccountService::getInstance()->syncOrderByAccount($accountId);
        }catch(\RuntimeException $e)
        {
            $this->error($e->getMessage());
        }
        $this->success('同步中，稍后请到订单列表中查看....');
    }

    /**
     * 删除
     * @date 2020/09/17
     * @author longli
     */
    public function delete()
    {
        $accountId = $this->request->get("account_id");
        if(empty($accountId)) $this->error('非法操作');
        AccountSettings::destroy(['account_id' => $accountId]);
        $this->success('删除成功');
    }

    /**
     * 编辑
     * @return string
     * @date 2020/09/17
     * @author longli
     */
    public function edit()
    {
        $accountId = $this->request->get("account_id");
        $this->assign("status", AccountSettings::$STATUS);
        $this->assign("auto", AccountSettings::$AUTO_SYNC);
        if(!empty($accountId))
        {
            $this->assign("account", Account::with(['settings'])->get($accountId));
        }
        return $this->fetch("edit");
    }
}