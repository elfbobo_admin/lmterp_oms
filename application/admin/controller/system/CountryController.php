<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/20
 * Time: 16:08
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\system;


use app\admin\controller\BaseController;
use app\common\model\Countries;
use think\facade\Validate;
use app\common\service\system\SystemService;

class CountryController extends BaseController
{
    /**
     * 国家首页
     * @return string
     * @date 2020/09/20
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $country = Countries::order("name_en, country_id desc")->select();
            $this->assign("list", $country);
            return $this->fetch("lists");
        }
        return $this->fetch("index");
    }

    /**
     * 添加国家
     * @return array
     * @date 2020/09/20
     * @author longli
     */
    public function add()
    {
        $this->assign("continent", Countries::field(["continent_en", "continent_ch"])->group("continent_ch")->select());
        $countryId = $this->request->request("country_id");
        if(!empty($countryId)) $this->assign("country", Countries::get($countryId));
        return $this->fetch("add");
    }

    /**
     * 更新国家信息
     * @return array
     * @date 2020/09/20
     * @author longli
     */
    public function update()
    {
        if($this->request->isPost())
        {
            $validate = Validate::make([
                'name_ch'  => 'require|unique:countries,name_ch',
                'name_en'  => 'require|unique:countries,name_en',
                'code_two'  => 'require|unique:countries,code_two|max:2',
                'code_three'  => 'require|unique:countries,code_three|max:3',
                'num_code'  => 'max:3',
                'currency_code'  => 'require',
                'currency_name'  => 'require',
                'continent_en'  => 'require',
            ],[
                'name_ch.require' => '中文名必填',
                'name_en.require' => '英文名必填',
                'code_two.require' => '二字代码必填',
                'code_three.require' => '三字代码必填',
                'currency_code.require' => '币种代码必填',
                'currency_name.require' => '币种名称必填',
                'continent_en.require' => '所属洲必选',
                'name_ch.unique' => '中文名称已存在',
                'name_en.unique' => '英文名称已存在',
                'code_two.unique' => '二字代码已存在',
                'code_three.unique' => '三字代码已存在',
                'code_two.max' => '二字代码只能填二位',
                'num_code.max' => '数字代码只能填三位',
                'code_three.max' => '三字代码只能填三位',
            ]);
            $data = $this->request->post();
        }
        else
        {
            $validate = Validate::make([
                'country_id'  => 'require',
            ],[
                'country_id.require' => '非法请求',
            ]);
            $data = $this->request->get();
        }
        if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        SystemService::getInstance()->addCountry($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 删除国家
     * @return array
     * @date 2020/09/20
     * @author longli
     */
    public function delete()
    {
        $countryId = $this->request->request("ids");
        if(empty($countryId)) $this->error('参数错误');
        Countries::destroy($countryId);
        $this->success('删除成功');
    }
}