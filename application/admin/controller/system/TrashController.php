<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/18
 * Time: 15:56
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\system;


use app\admin\controller\BaseController;
use app\common\model\Admin;
use app\common\model\SysTrash;
use app\common\service\logistics\ChannelService;
use app\common\service\system\SystemService;

class TrashController extends BaseController
{
    /**
     * 回收站列表
     * @date 2020/09/18
     * @author longli
     */
    public function index()
    {
        // @todo 未没测试，还有数据恢复有问题
        $this->assign("admin", Admin::getAll());
        if(!$this->request->isAjax()) return $this->fetch('index');
        $model = SysTrash::with(["admin"])->order("trash_id desc");

        // 处理搜索
        $this->search($model);

        $limit = ChannelService::getInstance()->getPageSize($this->request->request());
        $ret = $model->paginate($limit);
        $this->assign('list', $ret->getCollection());
        $this->assign('page', $ret->render());
        return $this->fetch('lists');
    }

    /**
     * 处理搜索
     * @param SysTrash $model 回收站模型
     * @date 2020/09/18
     * @author longli
     */
    private function search($model)
    {
        $aid = $this->request->request('admin_id', '', 'trim');
        $tablename = $this->request->request('tablename', '', 'trim');
        $date = $this->request->request('date', '', 'trim');

        if(!empty($aid)) $model-where("create_by", $aid);
        if(!empty($tablename)) $model->where('tablename', $tablename);
        if(!empty($date))
        {
            $model->where(SystemService::getInstance()->parseLayuiRangeDate('create_time', $date));
        }
    }

    /**
     * 恢复回收站数据
     * @date 2020/09/18
     * @author longli
     */
    public function recover()
    {
        $trashId = $this->request->request("ids");
        if(empty($trashId)) $this->error('参数错误');
        SystemService::getInstance()->recover($trashId)
            ? $this->success("恢复成功")
            : $this->error("恢复失败");
    }

    /**
     * 删除回收站
     * @date 2020/09/18
     * @author longli
     */
    public function delete()
    {
        $trashId = $this->request->request("ids");
        if(empty($trashId)) $this->error('参数错误');
        SysTrash::destroy($trashId);
        $this->success('删除成功');
    }
}