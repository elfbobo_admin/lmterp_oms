<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/07/30
 * Time: 13:52
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller;

use think\Controller;

abstract class BaseController extends Controller
{
    /**
     * 初始化
     */
    public function initialize()
    {
        parent::initialize();
    }
}