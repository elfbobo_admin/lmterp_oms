<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/15
 * Time: 10:16
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;


use app\admin\controller\BaseController;
use app\common\model\ProductSpec;
use app\common\service\product\ProductService;
use app\common\status\BaseStatus;
use think\facade\Validate;

class SpecController extends BaseController
{
    /**
     * 首页
     * @return string
     * @date 2020/09/15
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $where = [];
            $name = $this->request->request("name", '', 'trim');
            $type = $this->request->request("type", '', 'trim');
            if(!empty($name)) $where[] = ["name", "like", "%{$name}%"];
            if(!empty($type)) $where[] = ["type", "=", $type];
            $limit = ProductService::getInstance()->getPageSize($this->request->request());
            $spec = ProductSpec::where($where)->order("type, sort desc")->paginate($limit);
            $this->assign("list", $spec->getCollection());
            $this->assign("page", $spec->render());
            return $this->fetch('lists');
        }
        $this->assign("spec_group", ProductSpec::group("type")->select());
        return $this->fetch("index");
    }

    /**
     * 添加规格
     * @return array
     * @date 2020/09/15
     * @author longli
     */
    public function add()
    {
        $specId = $this->request->get("spec_id");
        if(!empty($specId)) $this->assign("spec", ProductSpec::get($specId));
        return $this->fetch("add");
    }

    /**
     * 更新规格信息
     * @return array
     * @date 2020/09/15
     * @author longli
     */
    public function update()
    {
        if($this->request->isPost())
        {
            $validate = Validate::make([
                'name_ch'  => 'require',
                'type' => 'require',
                'name' => 'require',
                'value' => 'require',
            ],[
                'name_ch.require' => '分类名必填',
                'type.require' => '类别必填',
                'name.require' => '属性必填',
                'value.require' => '值必填',
            ]);
            $data = $this->request->post();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            if(!isset($data['status'])) $data['status'] = ProductSpec::STATUS_N;
        }
        else
        {
            $validate = Validate::make([
                'spec_id'  => 'require',
            ],[
                'spec_id.require' => '非法请求',
            ]);
            $data = $this->request->get();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        ProductService::getInstance()->addSpec($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 删除规格
     * @return array
     * @date 2020/09/15
     * @author longli
     */
    public function delete()
    {
        $specId = $this->request->request("ids");
        if(empty($specId)) $this->error('参数错误');
        ProductSpec::destroy($specId);
        $this->success('删除成功');
    }
}