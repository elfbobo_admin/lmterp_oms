<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/15
 * Time: 15:39
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;


use app\admin\controller\BaseController;
use app\common\library\Tools;
use app\common\model\Account;
use app\common\model\AccountPlatform;
use app\common\model\ProductPlatformSku;
use app\common\model\ProductStore;
use app\common\service\product\PlatformService;
use app\common\status\BaseStatus;

class PlatformController extends BaseController
{
    /**
     * 平台sku列表
     * @return string|string[]
     * @date 2020/09/15
     * @author longli
     */
    public function index()
    {
        $this->assign('select_product', [
            'sku' => 'SKU',
            'platform_sku' => '平台SKU',
            'name_ch' => '商品中文名',
            'name_en' => '商品英文名',
            'producer_name' => '供应商',
        ]);
        $this->assign('platform', AccountPlatform::getAll());
        $this->assign('account', Account::getAll());
        $this->assign("status", ProductPlatformSku::$STATUS);
        $this->assign('create_date', [-1 => '昨天', 0 => '今天', -6 => '7天前', -59 => '60天前']);
        $this->assign('sort', ['product_id' => '生成时间', 'name_ch' => '名称']);
        if($this->request->isAjax())
        {
            $pskus = PlatformService::getInstance()->search($this->request->request());
            return apiResponsePage($pskus);
        }
        return $this->fetch("index");
    }

    public function readd()
    {
        $pid = $this->request->request("pid");
        if(empty($pid)) return apiResponse(BaseStatus::CODE_FAULT, [], '参数错误');
        return PlatformService::getInstance()->generateSkuByPid($pid)
                ? apiResponse(BaseStatus::CODE_NORMAL, [], "生成成功")
                : apiResponse(BaseStatus::CODE_FAULT, [], "生成失败");
    }

    /**
     * 删除平台SKU
     * @return array
     * @date 2020/09/15
     * @author longli
     */
    public function delete()
    {
        $pid = $this->request->request("ids");
        if(empty($pid)) return apiResponse(BaseStatus::CODE_FAULT, [], '参数错误');
        ProductPlatformSku::destroy($pid);
        return apiResponse(BaseStatus::CODE_NORMAL, [], '删除成功');
    }

    /**
     * 导出文件
     * @return array
     * @date 2020/09/13
     * @author longli
     */
    public function export()
    {
        $ids = $this->request->request("ids");
        $startDate = $this->request->request("start_date");
        $accountId = $this->request->request("account_id");
        $platform = $this->request->request("platform");
        if(empty($ids) && empty($startDate)) return apiResponse(BaseStatus::CODE_FAULT, [], "非法导出，请检查导出条件");
        if(!empty($platform))
        {
            $platform = AccountPlatform::get(['name' => $platform]);
            if(empty($platform)) return apiResponse(BaseStatus::CODE_FAULT, [], "选择的平台不存在，请检查");
            $accountId = array_column($platform->account->toArray(), 'account_id');
        }
        $condition = [];
        if(!empty($ids)) $condition[] = ['sku.pid', 'in', $ids];
        if(!empty($startDate)) $condition = array_merge($condition, PlatformService::getInstance()->parseLayuiRangeDate("sku.create_time", $startDate));
        if(!empty($accountId)) $condition[] = ['sku.account_id', 'in', $accountId];

        $skuExport = new \app\common\service\export\PlatformSku();
        $file = $skuExport->runExcel($condition);
        return apiResponse(BaseStatus::CODE_NORMAL, ["src" => urlencode($file)], "导出成功");
    }

    /**
     * 导入商品
     * @return array
     * @date 2020/09/13
     * @author longli
     */
    public function import()
    {
        $file = $this->request->request("path");
        if(Tools::startWith($file, '/')) $file = \Env::get("root_path") . "public{$file}";
        $skuImport = new \app\common\service\import\PlatformSku($file);
        try
        {
            $skuImport->run();
            return apiResponse(BaseStatus::CODE_NORMAL, [], '导入成功');
        }catch(\RuntimeException $e)
        {
            return apiResponse(BaseStatus::CODE_FAULT, [], $e->getMessage());
        }
    }

    /**
     * 生成平台sku
     * @return array
     * @date 2020/09/15
     * @author longli
     */
    public function generatesku()
    {
        $accountId = $this->request->request("account_id");
        $productId = $this->request->request("product_id");
        $storeId = $this->request->request("store_id");
        if(empty($accountId) || (empty($productId) && empty($storeId))) return apiResponse(BaseStatus::CODE_FAULT, [], "非法请求");
        if(!empty($productId))
        {
            $t = ProductStore::field("store_id")
                ->where("product_id", "in", $productId)
                ->select()->toArray();
            $storeId = array_column($t, 'store_id');
        }
        if(empty($storeId)) return apiResponse(BaseStatus::CODE_FAULT, [], "未选择要生成SKU的商品");
        return PlatformService::getInstance()->generateSkuByStoreId($accountId, $storeId)
                ? apiResponse(BaseStatus::CODE_NORMAL, [], "生成成功")
                : apiResponse(BaseStatus::CODE_FAULT, [], "生成失败");
    }
}