<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/07
 * Time: 13:29
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;


use app\admin\controller\BaseController;
use app\common\model\Warehouse;
use app\common\service\product\WarehouseService;
use think\facade\Validate;

class WarehouseController extends BaseController
{
    public function index()
    {
        if($this->request->isAjax())
        {
            $warehouse = Warehouse::order("warehouse_id desc")->select();
            $this->assign("list", $warehouse);
            return $this->fetch("lists");
        }
        return $this->fetch("index");
    }

    /**
     * 添加仓库
     * @return array
     * @date 2020/09/14
     * @author longli
     */
    public function add()
    {
        $wid = $this->request->request('warehouse_id');
        if(!empty($wid)) $this->assign('warehouse', Warehouse::get($wid));
        return $this->fetch('add');
    }

    /**
     * 更新仓库信息
     * @return array
     * @date 2020/09/14
     * @author longli
     */
    public function update()
    {
        if($this->request->isPost())
        {
            $validate = Validate::make([
                'name'  => 'require',
                'area'  => 'egt:0',
            ],[
                'name.require' => '仓库名称必填',
                'area.egt' => '仓库面积无效',
            ]);
            $data = $this->request->post();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            if(!isset($data['status'])) $data['status'] = Warehouse::STATUS_N;
        }
        else
        {
            $validate = Validate::make([
                'warehouse_id'  => 'require',
            ],[
                'warehouse_id.require' => '非法请求',
            ]);
            $data = $this->request->get();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        WarehouseService::getInstance()->addWarehouse($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 删除仓库
     * @return array
     * @date 2020/09/14
     * @author longli
     */
    public function delete()
    {
        $warehouseId = $this->request->request("ids");
        if(empty($warehouseId)) $this->error('参数错误');
        Warehouse::destroy($warehouseId);
        $this->success('删除成功');
    }
}