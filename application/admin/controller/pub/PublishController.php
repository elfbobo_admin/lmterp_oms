<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/07
 * Time: 13:29
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\pub;


use app\admin\controller\BaseController;

class PublishController extends BaseController
{
    public function index()
    {
        return $this->error("功能在研发中");
    }
}