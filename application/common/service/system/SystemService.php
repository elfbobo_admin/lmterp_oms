<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/18
 * Time: 22:28
 * @link http://www.lmterp.cn
 */

namespace app\common\service\system;


use app\common\library\Tools;
use app\common\model\Countries;
use app\common\model\Exchange;
use app\common\model\SysTrash;
use app\common\service\BaseService;
use think\Db;
use think\facade\Log;

class SystemService extends BaseService
{
    /**
     * 清空回收站指定天数的数据
     * @param int $day 具体的天数，默认 60天
     * @return bool
     * @date 2020/09/18
     * @author longli
     */
    public function clearTrash($day = 60)
    {
        if($day < 0) return false;
        $date = date('Y-m-d', strtotime("-{$day}day"));
        try
        {
            return SysTrash::where("create_time", "<=", $date)->delete() > 0;
        } catch (\Exception $e){}
        return false;
    }

    /**
     * 恢复回收站数据
     * @param int|int[] $ids 回收站 id
     * @return bool
     * @date 2020/09/18
     * @author longli
     */
    public function recover($ids = [])
    {
        if(!is_array($ids)) $ids = explode(",", $ids);
        try
        {
            Db::startTrans();
            foreach(SysTrash::where("trash_id", "in", $ids)->select() as $item)
            {
                Db::table($item->tablename)->insert(unserialize($item->data), true);
                $item->delete();
            }
            Db::commit();
            return true;
        }catch(\Exception $e)
        {
            Db::rollback();
            Log::info(sprintf("回收站恢复失败，失败信息【%s】", $e->getMessage()));
        }
        return false;
    }

    /**
     * 添加国家
     * @param array $data 国家信息
     * @return Countries|bool
     * @date 2020/09/20
     * @author longli
     */
    public function addCountry($data = [])
    {
        $cData = Countries::getFilterField($data);
        if(empty($cData)) return false;
        $cData = Tools::trim($cData);
        $country = null;
        foreach(['code_two', 'code_three', 'currency_code'] as $v)
            if(isset($cData[$v])) $cData[$v] = strtoupper($cData[$v]);
        if(isset($cData['country_id'])) $country = Countries::get($cData['country_id']);
        if(!empty($country))
        {
            $country->save($cData);
        }
        else
        {
            if(isset($cData['continent_en']))
                $cData['continent_ch'] = Countries::get(['continent_en'=>$cData['continent_en']])->continent_ch;
            $country = Countries::create($cData);
        }
        return $country;
    }

    /**
     * 添加汇率
     * @param array $data 汇率信息
     * @return Exchange|bool
     * @date 2020/09/20
     * @author longli
     */
    public function addExchange($data = [])
    {
        $eData = Exchange::getFilterField($data);
        if(empty($eData)) return false;
        if(isset($eData['source_code']) && empty($eData['source_name']))
            $eData['source_name'] = Countries::get(['currency_code' => $eData['source_code']])->currency_name;
        if(isset($eData['target_code']) && empty($eData['target_name']))
            $eData['target_name'] = Countries::get(['currency_code' => $eData['target_code']])->currency_name;
        $exchange = null;
        if(isset($eData['exchange_id'])) $exchange = Exchange::get($eData['exchange_id']);
        if(empty($exchange) && isset($eData['source_code']) && $eData['target_code'])
            $exchange = Exchange::get(['source_code' => $eData['source_code'], 'target_code' => $eData['target_code']]);
        if(!empty($exchange))
        {
            $exchange->save($eData);
        }
        else
        {
            $exchange = Exchange::create($eData);
        }
        return $exchange;
    }

    /**
     * 汇率兑换
     * @param string $sourceCode 源兑换币种代码
     * @param string $targetCode 目标兑换种代码
     * @param float $money 需要兑换的金额
     * @return float 如果返回 0 则是没有兑换成功
     * @date 2020/09/20
     * @author longli
     */
    public function exchangeRate($sourceCode, $targetCode, $money = 1.0)
    {
        if($sourceCode == $targetCode) return $money;
        if(!($e = Exchange::get(['source_code' => $sourceCode, 'target_code' => $targetCode]))) return 0;
        return $e->rate * $money;
    }
}