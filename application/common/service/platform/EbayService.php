<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/04/17
 * Time: 15:56
 * @link http://www.lmterp.cn
 */

namespace app\common\service\platform;


use app\common\model\OrdersTemp;

/**
 * Ebay 接口服务
 * Class EbayService
 * @package app\common\service\ebay
 * @link https://developer.ebay.com/devzone/xml/docs/reference/ebay/getorders.html
 */
class EbayService extends BasePlatformService
{
    /**
     * @inheritDoc
     */
    public function syncOrder(OrdersTemp $ordersTemp)
    {
        // TODO: Implement syncOrder() method.
    }

    public function getOrderList($params = [], $replace = false)
    {
        // TODO: Implement getOrderList() method.
    }
}