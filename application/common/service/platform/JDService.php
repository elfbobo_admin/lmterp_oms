<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/05/13
 * Time: 10:27
 * @link http://www.lmterp.cn
 */

namespace app\common\service\platform;


use app\common\model\OrdersTemp;

/**
 * 京东接口服务
 * Class JDService
 * @package app\common\service\jingdong
 * @link https://help.jd.com/jos
 */
class JDService extends BasePlatformService
{

    /**
     * @inheritDoc
     */
    public function syncOrder(OrdersTemp $ordersTemp)
    {
        // TODO: Implement syncOrder() method.
    }

    public function getOrderList($params = [], $replace = false)
    {
        // TODO: Implement getOrderList() method.
    }
}