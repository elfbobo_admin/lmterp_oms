<?php
/**
 * Created by Administrator
 * User: longli
 * Date: 2020/08/12
 * Time: 10:52
 * @link http://www.lmterp.cn
 */

namespace app\common\service\platform;


use app\common\library\Tools;
use app\common\model\AccountSyncLog;
use app\common\model\Orders;
use app\common\model\OrdersTemp;
use app\common\service\orders\OrderService;
use GuzzleHttp\Client;
use think\facade\Log;

/**
 * worten 接口服务 开发文档需要登录才能看到
 * Class WortenService
 * @package app\common\service\worten
 * @link https://marketplace.worten.pt
 */
class WortenService extends BasePlatformService
{
    /**
     * HTTP 客户端
     * @var Client
     */
    protected $client;

    public function init()
    {
        $this->client = new Client(['base_uri' => $this->getBaseUrl(), 'timeout' => 10]);
    }

    /**
     * 获取订单列表
     * @param array $params 参数配置，默认抓取当天订单
     * <ul>
     * <li>string start_date 开始抓取时间，默认为今天</li>
     * <li>string end_date 结束抓取时间，默认为当前时间</li>
     * <li>int status 订单状态，默认为 SHIPPING</li>
     * </ul>
     * @param bool $replace 是否替换原有的订单，默认不替换
     * @date 2020/08/15
     * @author longli
     * @link https://marketplace.worten.pt/api/orders
     */
    public function getOrderList($params = [], $replace = false)
    {
        Log::info(sprintf("worten 批量同步账号【%d】订单，请求参数: 【%s】", $this->getAccountId(), json_encode($params)));
        $startSyncTime = time();
        // 添加同步订单日志
        AccountSyncLog::addLog($this->getAccountId(), $startSyncTime, time(), $params);
    }

    public function markDelivery($orderIds = [])
    {
        $retCode = true;
        return $retCode;
    }

    /**
     * 获取请求头信息
     * @date 2020/08/15
     * @author longli
     * @return string[]
     */
    public function getHeader()
    {
        $account = $this->getAccount();
        return [
            "Content-type" => "application/json",
            "Accept" => "application/json",
            "Authorization" => $account->token->secret,
        ];
    }


    /**
     * @inheritDoc
     */
    public function syncOrder(OrdersTemp $ordersTemp)
    {
        $info = [];
        $order = [
            "order_detail" => $info,
        ];
        return OrderService::getInstance()->addOrder($order);
    }
}