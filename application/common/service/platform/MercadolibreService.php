<?php
/**
 * Created by Administrator
 * User: longli
 * Date: 2020/08/17
 * Time: 19:44
 * @link http://www.lmterp.cn
 */

namespace app\common\service\platform;

use app\common\model\OrdersTemp;

/**
 * MercadoLibre 美客多服务接口
 * Class MercadolibreService
 * @package app\common\service\platform
 * @link https://developers.mercadolivre.com.br/en_us/authentication-and-authorization
 * @link https://global-selling.mercadolibre.com/devsite/orders#close
 */
class MercadolibreService extends BasePlatformService
{
    // @todo 没有任何对接
    public function syncOrder(OrdersTemp $ordersTemp)
    {
        // TODO: Implement syncOrder() method.
    }

    /**
     * 刷新 token
     * @date 2020/08/21
     * @author longli
     * @return bool
     */
    public function refreshToken()
    {
        return true;
    }

    public function getOrderList($params = [], $replace = false)
    {
        // TODO: Implement getOrderList() method.
    }
}