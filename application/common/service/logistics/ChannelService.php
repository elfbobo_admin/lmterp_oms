<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/04
 * Time: 17:21
 * @link http://www.lmterp.cn
 */

namespace app\common\service\logistics;

use app\common\library\Tools;
use app\common\model\Channel;
use app\common\model\ChannelCarrier;
use app\common\model\ChannelOrders;
use app\common\model\ChannelSender;
use app\common\model\Orders;
use app\common\service\BaseService;
use app\common\service\channel\BaseChannel;
use think\facade\Log;

class ChannelService extends BaseService
{
    /**
     * 保存订单预报信息
     * @param array $data
     * @return ChannelOrders|null
     * @date 2020/09/05
     * @author longli
     */
    public function saveChannelOrders($data = [])
    {
        $insert = ChannelOrders::getFilterField($data);
        if(empty($insert) || empty($insert['channel_id']) || empty($insert['order_id'])) return null;
        $chOrder = null;
        if(!empty($insert['ch_id'])) $chOrder = ChannelOrders::get($insert['ch_id']);
        if(empty($chOrder) && !empty($insert['track_number']))
            $chOrder = ChannelOrders::get(['channel_id' => $insert['channel_id'], 'order_id' => $insert['order_id'], 'track_number' => $insert['track_number']]);
        if(!empty($chOrder))
        {
            $chOrder->save($insert);
        }
        else
        {
            ChannelOrders::where(['channel_id' => $insert['channel_id'], 'order_id' => $insert['order_id']])->update(['is_cancel' => ChannelOrders::CANCEL_Y]);
            $chOrder = ChannelOrders::create($insert);
        }
        return $chOrder;
    }

    /**
     * 添加渠道
     * @param array $data 渠道信息
     * @return Channel|bool
     * @date 2020/09/18
     * @author longli
     */
    public function addChannel($data = [])
    {
        $cData = Channel::getFilterField($data);
        if(empty($cData)) return false;
        $cData = Tools::trim($cData);
        $channel = null;
        if(isset($cData['channel_id'])) $channel = Channel::get($cData['channel_id']);
        if(!empty($channel))
        {
            $channel->save($cData);
        }
        else
        {
            $channel = Channel::create($cData);
        }
        return $channel;
    }

    /**
     * 添加承运商
     * @param array $data 承运商信息
     * @return ChannelCarrier|bool
     * @date 2020/09/18
     * @author longli
     */
    public function addCarrier($data = [])
    {
        $cData = ChannelCarrier::getFilterField($data);
        if(empty($cData)) return false;
        $cData = Tools::trim($cData);
        $carrier = null;
        if(isset($cData['carrier_id'])) $carrier = ChannelCarrier::get($cData['carrier_id']);
        if(!empty($carrier))
        {
            $carrier->save($cData);
        }
        else
        {
            $carrier = ChannelCarrier::create($cData);
        }
        return $carrier;
    }

    /**
     * 添加发货人
     * @param array $data 发货人信息
     * @return ChannelSender|bool
     * @date 2020/09/18
     * @author longli
     */
    public function addSender($data = [])
    {
        $sData = ChannelSender::getFilterField($data);
        if(empty($sData)) return false;
        $sData = Tools::trim($sData);
        $sender = null;
        if(isset($sData['sender_id'])) $sender = ChannelSender::get($sData['sender_id']);
        if(!empty($sData['is_default'])) ChannelSender::where('sender_id', '>', 0)->update(['is_default' => ChannelSender::DEFAULT_N]);
        if(!empty($sender))
        {
            $sender->save($sData);
        }
        else
        {
            $sender = ChannelSender::create($sData);
        }
        return $sender;
    }

    /**
     * 申报订单获取追踪号和面单
     * @param int $channelId 渠道 id
     * @param int|Orders $order 订单 id 或者订单实体
     * @return bool
     * @date 2020/09/25
     * @author longli
     */
    public function declareOrder($channelId, $order)
    {
        if(!($obj = $this->buildChannelClasses($channelId))) return false;
        if(is_numeric($order)) $order = Orders::get($order);
        $order->order_status = Orders::ORDER_DECLARE_ING;
        $order->save();
        $obj->setOrder($order);
        $obj->getTrackNumber();
        $obj->getLabel();
        $order->order_status = Orders::ORDER_PACKAGE;
        $order->save();
        return true;
    }

    /**
     * 取消申报订单
     * @param int $channelId 渠道 id
     * @param int|Orders $order 订单 id 或者订单实体
     * @return bool
     * @date 2020/09/25
     * @author longli
     */
    public function cancelOrder($channelId, $order)
    {
        if(!($obj = $this->buildChannelClasses($channelId))) return false;
        if(is_numeric($order)) $order = Orders::get($order);
        $order->channel_id = 0;
        $obj->setOrder($order);
        if($t = $obj->cancelOrder())
        {
            $obj->resetOrderStatus();
        }
        return $t;
    }

    /**
     * 构建渠道对接类
     * @param int|Channel $channel 渠道id, 或者渠道实体
     * @return bool|BaseChannel
     * @date 2020/09/25
     * @author longli
     */
    public function buildChannelClasses($channel)
    {
        if(is_numeric($channel)) $channel = Channel::get($channel);
        if(empty($channel)) return false;
        $classes = BaseChannel::getClasses($channel->classes);
        try
        {
            return new $classes($channel->channel_id);
        }catch(\Exception $e)
        {
            Log::info($e->getMessage());
        }
        return false;
    }
}