<?php


namespace app\common\service;


use app\common\library\Tools;
use app\common\model\Account;
use think\facade\Cache;

abstract class BaseService
{

    protected static $instance = null;

    /**
     * 获取单例对象
     * @date 2020/08/12
     * @author longli
     * @return static
     */
    public static function getInstance()
    {
        if(!(static::$instance instanceof static)) static::$instance = new static();
        return self::$instance;
    }

    /**
     * 通过 ID 获取平台账号信息
     * @param int $accountId 账号 ID
     * @return Account
     * @date 2020/02/25
     * @author longli
     */
    public function getAccountById($accountId)
    {
        // @todo 开发时不启用缓存，发布需要开启
        return Account::getOrFail($accountId);
        return Cache::remember("service_account_id_$accountId", function() use($accountId)
        {
            return Account::getOrFail($accountId);
        }, 3600);
    }

    /**
     * 解析时间范围到 where 条件中
     * @param string $field 字段
     * @param int $date 时间，指定天数，默认为今天
     * @return array
     * @date 2020/09/16
     * @author longli
     */
    public function parseScopeDateToWhere($field, $date = 0)
    {
        $where = [];
        $pdate = date('Y-m-d', strtotime("{$date}day"));
        if($date == 0)
        {
            $where[] = [$field, '>=', $pdate];
        }
        else if($date == -1)
        {
            $where[] = [$field, '>=', $pdate];
            $where[] = [$field, '<', date('Y-m-d')];
        }
        else
        {
            $where[] = [$field, "<=", $pdate];
        }
        return $where;
    }

    /**
     * 解析 layui date 时间范围
     * @param string $field 字段
     * @param string $date layui 时间格式
     * @return array
     * @date 2020/09/17
     * @author longli
     */
    public function parseLayuiRangeDate($field = '', $date = '')
    {
        if(empty($field) && empty($date)) return [];
        $st = explode(' ~ ', $date);
        if(empty($field)) return $st;
        return [
            [$field, ">=", $st[0]],
            [$field, "<=", $st[1]]
        ];
    }

    /**
     * 获取分页大小
     * @param array $search
     * @return int|null
     * @date 2020/09/15
     * @author longli
     */
    public function getPageSize($search = [])
    {
        $perhaps = ['limit', 'page_size', 'pageSize'];
        foreach($perhaps as $v)
        {
            if(!empty($search[$v])) return $search[$v];
        }
        return null;
    }

    /**
     * 获取指定路径下的所有 Service 服务
     * @param string $path 路径
     * @param string|string[] $filterClass 需要排除的类
     * @return array
     * @date 2020/09/24
     * @author longli
     */
    public static function getAllService($path, $filterClass = [])
    {
        if(!is_array($filterClass)) $filterClass = [$filterClass];
        $classes = array_map(function($item)
        {
            list($name) = explode('.', substr(strrchr($item, '/'), 1));
            return $name;
        }, Tools::getFiles($path));
        if(empty($filterClass)) return $classes;
        return array_filter($classes, function($item) use ($filterClass)
        {
            return !in_array($item, $filterClass);
        });
    }
}