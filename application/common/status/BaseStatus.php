<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/01
 * Time: 14:45
 * @link http://www.lmterp.cn
 */

namespace app\common\status;


class BaseStatus
{
    /**
     * 成功
     */
    const CODE_NORMAL = 0;

    /**
     * 失败
     */
    const CODE_FAULT = 1;
}