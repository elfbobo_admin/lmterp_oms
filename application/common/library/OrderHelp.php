<?php
/**
 * Created by PhpStorm.
  * User: longli
 * Date: 2020/04/14
 * Time: 15:47
 * @link http://www.lmterp.cn
 */

namespace app\common\library;


class OrderHelp
{
    /**
     * 生成内部唯一订单号
     * @param string $platformCode 平台编码
     * @param string $siteCode 站点编码
     * @param string $num 第几个订单, 1-9, a-z 可代表的订单数，共35个
     * @return string
     * @date 2020/04/14
     * @author longli
     */
    public static function generatorOrderSn($platformCode, $siteCode = '', $num = '1')
    {
        $platformCode = strtoupper($platformCode);
        $siteCode = strtoupper($siteCode);
        return "{$platformCode}{$siteCode}{$num}-" . date('YmdHis') . mt_rand(10000, 99999);
    }
}