<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/27
 * Time: 22:22
 * @link http://www.lmterp.cn
 */


namespace app\common\model;

use think\Model;

class ReportOrderDay extends BaseModel
{
    protected $pk = 'day_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $updateTime = false;
}
