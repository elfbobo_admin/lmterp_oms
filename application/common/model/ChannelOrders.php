<?php

namespace app\common\model;

class ChannelOrders extends BaseModel
{
    protected $pk = 'ch_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 取消
     */
    const CANCEL_Y = 1;

    /**
     * 正常
     */
    const CANCEL_N = 0;

    public static $CANCEL = [
        self::CANCEL_Y => '取消',
        self::CANCEL_N => '正常',
    ];


    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    /**
     * 关联订单
     * @return \think\db\Query|\think\model\relation\BelongsTo
     * @date 2020/09/18
     * @author longli
     */
    public function order()
    {
        return $this->belongsTo(Orders::class, 'order_id', 'order_id');
    }

    /**
     * 关联渠道
     * @return \think\model\relation\BelongsTo
     * @date 2020/09/18
     * @author longli
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }
}
