<?php

namespace app\common\model;


class Countries extends BaseModel
{
    protected $pk = 'country_id';

    /**
     * 通过国家编码获取一条数据
     * @param string $code 国家二字简码或者三字简码
     * @return Countries
     * @date 2020/08/28
     * @author longli
     */
    public static function getByCode($code = '')
    {
        $code = trim($code);
        $field = strlen($code) == 2 ? 'code_two' : 'code_three';
        return static::get([$field => $code]);
    }

    /**
     * 通过货币代码获取一条数据
     * @param string $code 货币代码
     * @return Countries
     * @date 2020/08/28
     * @author longli
     */
    public static function getByCurrencyCode($code = '')
    {
        return static::get(["currency_code" => trim($code)]);
    }

    /**
     * 通过名称获取一条数据
     * @param string $name 国家英文或者中文名称
     * @return Countries
     * @date 2020/08/28
     * @author longli
     */
    public static function getByName($name = '')
    {
        $name = trim($name);
        $field = preg_match('/^[a-z]+$/i', $name)
            ? "name_en"
            : "name_ch";
        return static::get([$field => $name]);
    }
}
