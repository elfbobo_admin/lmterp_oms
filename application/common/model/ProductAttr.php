<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/08/15
 * Time: 20:25
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


class ProductAttr extends BaseModel
{
    protected $pk = 'attr_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}