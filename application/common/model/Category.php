<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/08/25
 * Time: 15:24
 * @link http://www.lmterp.cn
 */
namespace app\common\model;

use app\common\library\Tools;
use Exception;
use think\Collection;

class Category extends BaseModel
{
    protected $pk = 'cate_id';

    /**
     * 启用
     */
    const STATUS_Y = 1;

    /**
     * 关闭
     */
    const STATUS_N = 0;

    public static $STATUS = [
        self::STATUS_Y => '启用',
        self::STATUS_N => '关闭'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'cate_id');
    }

    public function son()
    {
        return $this->hasMany(Category::class, 'parent_id', 'cate_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'cate_id');
    }

    protected function setStatusAttr($value)
    {
        $temp = array_flip(self::$STATUS);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS[$value]) ? self::$STATUS[$value] : $value;
    }

    /**
     * 通过名称获取分类
     * @param $name
     * @date 2020/09/11
     * @author longli
     */
    public static function getByName($name)
    {
        return static::where(['name' => $name])->find();
    }

    /**
     * 获取当前父级下的子类
     * @param int $pid 父级 id
     * @return array|PDOStatement|string|Collection
     * @date 2020/09/09
     * @author longli
     */
    public static function getByPid($pid = 0)
    {
        try
        {
            return static::where(['parent_id' => $pid, 'status' => self::STATUS_Y])->select();
        }catch (Exception $e)
        {
            return [];
        }
    }

    /**
     * 获取当前分类下的所有子类
     * @param int $pid 父类 id
     * @return array|PDOStatement|string|Collection
     * @date 2020/09/13
     * @author longli
     */
    public static function getAllSon($pid = 0)
    {
        $ids = [];
        if($pid > 0) $ids[] = $pid;
        do
        {
            $cates = static::where("parent_id", "in", $pid)->field("cate_id")->select()->toArray();
            if(!empty($cates))
            {
                $pid = array_column($cates, 'cate_id');
                $ids = array_merge($pid, $ids);
            }
        }while(!empty($cates));
        // if(empty($ids)) return [];
        return static::where("cate_id", "in", $ids)->select();
    }

    /**
     * 从当前分类向上获取所有父类
     * @param int $sonId 分类id
     * @return array
     * @date 2020/09/09
     * @author longli
     */
    public static function getParentAll($sonId)
    {
        $ret = [];
        do
        {
            $category = Category::get(['cate_id' => $sonId, 'status' => self::STATUS_Y]);
            if(!empty($category))
            {
                $sonId = $category->parent_id;
                $ret[] = $category->toArray();
            }
        }while(!empty($category) && $category->parent_id != 0);
        krsort($ret);
        return array_values($ret);
    }

    /**
     * 获取当前节点的兄弟节点
     * @param int $cateId 分类id
     * @return array|PDOStatement|string|Collection|null
     * @date 2020/09/09
     * @author longli
     */
    public static function getBrotherNode($cateId)
    {
        $cate = Category::get($cateId);
        if(empty($cate)) return null;
        try {
            return Category::where(['parent_id' => $cate->parent_id, 'status' => self::STATUS_Y])->select();
        }catch (Exception $e){}
        return null;
    }

    /**
     * 分类名称是否存在
     * @param string $name 分类名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function hasByName($name)
    {
        return !!static::where(['name' => $name])->count();
    }

    /**
     * 检查分类名是否唯一
     * @param string $name 分类名
     * @return bool
     * @date 2020/09/13
     * @author longli
     */
    public static function checkUnique($name)
    {
        return static::where(['name' => $name])->count() < 2;
    }

    /**
     * 创建树形分类
     * @param bool $isUserId 是否使用id做为下标， 默认不使用
     * @return array
     * @date 2020/09/13
     * @author longli
     */
    public static function buildTree($isUserId = false)
    {
        $categories = static::where(['status' => self::STATUS_Y])->field(['cate_id', 'parent_id', 'name'])->select()->toArray();
        return Tools::generateTree($categories, 'cate_id', 'parent_id', $isUserId);
    }

    /**
     * 更新分类信息
     * @param int|int[] $ids 分类 id
     * @param array $data 分类信息
     * @return bool
     * @date 2020/09/14
     * @author longli
     */
    public static function editByCategoryId($ids, $data)
    {
        try
        {
            foreach(static::where("cate_id", "in", $ids)->select() as $cate)
            {
                $cate->save($data);
            }
            return true;
        }catch(\Exception $e)
        {
            return false;
        }
    }
}
