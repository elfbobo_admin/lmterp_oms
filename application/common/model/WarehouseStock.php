<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/16
 * Time: 14:32
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class WarehouseStock extends BaseModel
{
    protected $pk = 'stock_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    /**
     * 关联变体
     * @return \think\model\relation\BelongsTo
     * @date 2020/09/16
     * @author longli
     */
    public function store()
    {
        return $this->belongsTo(ProductStore::class, 'store_id', 'store_id');
    }
}
