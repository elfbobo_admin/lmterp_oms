<?php

namespace app\common\model;


class Admin extends BaseModel
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = false;


    public function orders()
    {
        return $this->hasMany(Orders::class, "create_by", "id");
    }

    /**
     * 密码加密
     * @param $password
     * @return string
     */
    public static function encryptPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function logout()
    {
        return true;
    }

    /**
     * 获取所有后台账号
     * @return array|\PDOStatement|string|\think\Collection
     * @date 2020/09/18
     * @author longli
     */
    public static function getAll()
    {
        return static::where("status", self::IS_YES)->order("username")->select();
    }
}
