<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/08/15
 * Time: 20:24
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


use think\model\relation\BelongsTo;
use think\model\relation\HasMany;

class Product extends BaseModel
{
    protected $pk = 'product_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 可用
     */
    const STATUS_Y = 1;

    /**
     * 禁用
     */
    const STATUS_N = 5;
    /**
     * 部分平台禁用
     */
    const STATUS_PE = 10;
    /**
     * 无货源
     */
    const STATUS_NONE = 15;

    /**
     * 新品开发
     */
    const STATUS_NEW = 20;

    public static $STATUS = [
        self::STATUS_Y => '可用',
        self::STATUS_N => '禁用',
        self::STATUS_PE => '部分平台禁用',
        self::STATUS_NONE => '无货源',
        self::STATUS_NEW => '新品开发',
    ];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setStatusAttr($value)
    {
        $temp = array_flip(self::$STATUS);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS[$value]) ? self::$STATUS[$value] : $value;
    }

    /**
     * 变体
     * @return HasMany
     * @date 2020/09/11
     * @author longli
     */
    public function stores()
    {
        return $this->hasMany(ProductStore::class);
    }

    /**
     * 图片
     * @return HasMany
     * @date 2020/09/11
     * @author longli
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * 属性
     * @return HasMany
     * @date 2020/09/11
     * @author longli
     */
    public function attrs()
    {
        return $this->hasMany(ProductAttr::class);
    }

    /**
     * 采购
     * @return HasMany
     * @date 2020/09/11
     * @author longli
     */
    public function purchase()
    {
        return $this->hasMany(ProductPurchase::class);
    }

    /**
     * 用户
     * @return BelongsTo
     * @date 2020/09/11
     * @author longli
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class, "create_by", "id");
    }

    /**
     * 分类
     * @return BelongsTo
     * @date 2020/09/11
     * @author longli
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'cate_id');
    }

    /**
     * 计量单位
     * @return BelongsTo
     * @date 2020/09/11
     * @author longli
     */
    public function unit()
    {
        return $this->belongsTo(ProductUnit::class,'unit_id', 'unit_id');
    }

    /**
     * 品牌
     * @return BelongsTo
     * @date 2020/09/11
     * @author longli
     */
    public function sbrand()
    {
        return $this->belongsTo(ProductBrand::class, 'brand', 'brand_id');
    }

    /**
     * 关联平台sku
     * @return HasMany
     * @date 2020/09/16
     * @author longli
     */
    public function pskus()
    {
        return $this->hasMany(ProductPlatformSku::class);
    }

    /**
     * 关联产品库存
     * @return \think\model\relation\HasManyThrough
     * @date 2020/09/16
     * @author longli
     */
    public function stocks()
    {
        return $this->hasManyThrough(WarehouseStock::class,ProductStore::class,'product_id', 'store_id', 'product_id');
    }

    /**
     * 更新商品信息
     * @param int|int[] $ids 商品 id
     * @param array $data 商品信息
     * @return bool
     * @date 2020/09/14
     * @author longli
     */
    public static function editByProductId($ids, $data)
    {
        try
        {
            foreach(static::where("product_id", "in", $ids)->select() as $product)
            {
                $product->save($data);
            }
            return true;
        }catch(\Exception $e)
        {
            return false;
        }
    }
}