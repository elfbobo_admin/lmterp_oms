<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/08
 * Time: 13:52
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ProductKeyword extends BaseModel
{
    protected $pk = 'word_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 启用
     */
    const STATUS_Y = 1;

    /**
     * 关闭
     */
    const STATUS_N = 0;

    public static $STATUS = [
        self::STATUS_Y => '启用',
        self::STATUS_N => '关闭'
    ];

    /**
     * 普通
     */
    const TYPE_ORD = 0;
    /**
     * 敏感词
     */
    const TYPE_SEN = 5;
    /**
     * 热词
     */
    const TYPE_HOT = 10;

    public static $WORD_TYPE = [
        self::TYPE_HOT => '热词',
        self::TYPE_SEN => '敏感词',
        self::TYPE_ORD => '普通',
    ];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }
}
