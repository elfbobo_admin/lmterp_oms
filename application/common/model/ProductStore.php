<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/08/15
 * Time: 20:26
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


class ProductStore extends BaseModel
{
    protected $pk = 'store_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    /**
     * 获取 sku 获取产品信息
     * @param string $sku sku编码
     * @date 2020/08/18
     * @author longli
     * @return ProductStore
     */
    public static function getStoreBySku($sku)
    {
        return static::get(['sku' => $sku]);
    }

    /**
     * 关联产品
     * @return \think\model\relation\BelongsTo
     * @date 2020/09/16
     * @author longli
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * 关联库存
     * @return \think\model\relation\HasMany
     * @date 2020/09/16
     * @author longli
     */
    public function stock()
    {
        return $this->hasMany(WarehouseStock::class, 'store_id', 'store_id');
    }

    /**
     * 关联平台sku
     * @return \think\model\relation\HasMany
     * @date 2020/09/16
     * @author longli
     */
    public function pskus()
    {
        return $this->hasMany(ProductPlatformSku::class, 'store_id', 'store_id');
    }

    /**
     * 检查 sku 是否存在
     * @param string $sku 需要检查的 sku
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function hasSku($sku)
    {
        return !!static::where('sku', $sku)->count();
    }
}