<?php

namespace app\common\model;

use app\common\library\Tools;
use PDOStatement;
use think\Collection;

class Channel extends BaseModel
{
    protected $pk = 'channel_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json', 'token'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 邮政
     */
    const CHANNEL_TYPE_Y = 1;

    /**
     * 专线
     */
    const CHANNEL_TYPE_Z = 2;

    /**
     * 快递
     */
    const CHANNEL_TYPE_K = 3;

    /**
     * 其它
     */
    const CHANNEL_TYPE_Q = 4;

    public static $CHANNEL_TYPE = [
        self::CHANNEL_TYPE_Y => '邮政',
        self::CHANNEL_TYPE_Z => '专线',
        self::CHANNEL_TYPE_K => '快递',
        self::CHANNEL_TYPE_Q => '其它',
    ];

    /**
     * 可追踪
     */
    const IS_TRACK_Y = 1;

    /**
     * 不可追踪
     */
    const IS_TRACK_N = 0;

    public static $IS_TRACK = [
        self::IS_TRACK_Y => '可追踪',
        self::IS_TRACK_N => '不可追踪',
    ];


    /**
     * 预备追踪号
     */
    const TRACK_TYPE_Y = 1;

    /**
     * API获取追踪号
     */
    const TRACK_TYPE_A = 2;

    public static $TRACK_TYPE = [
        self::TRACK_TYPE_A => 'API获取追踪号',
        self::TRACK_TYPE_Y => '预备追踪号',
    ];

    /**
     * 状态开启
     */
    const STATUS_Y = 1;

    /**
     * 状态关闭
     */
    const STATUS_N = 0;

    /**
     * 测试
     */
    const STATUS_T = 2;

    public static $CHANNEL_STATUS = [
        self::STATUS_Y => '开启',
        self::STATUS_N => '关闭',
        self::STATUS_T => '测试',
    ];

    /**
     * 自制
     */
    const LABEL_TYPE_S = 1;

    /**
     * api 下载
     */
    const LABEL_TYPE_D = 2;

    /**
     * 面单制作方式
     * @var array
     */
    public static $LABEL_TYPE = [
        self::LABEL_TYPE_D => 'API下载',
        self::LABEL_TYPE_S => '自制',
    ];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setLabelTypeAttr($value)
    {
        $temp = array_flip(self::$LABEL_TYPE);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getLabelTypeAttr($value)
    {
        return isset(self::$LABEL_TYPE[$value]) ? self::$LABEL_TYPE[$value] : $value;
    }

    protected function setTrackTypeAttr($value)
    {
        $temp = array_flip(self::$TRACK_TYPE);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getTrackTypeAttr($value)
    {
        return isset(self::$TRACK_TYPE[$value]) ? self::$TRACK_TYPE[$value] : $value;
    }

    protected function getChannelTypeAttr($value)
    {
        return isset(self::$CHANNEL_TYPE[$value]) ? self::$CHANNEL_TYPE[$value] : $value;
    }

    protected function setChannelTypeAttr($value)
    {
        $temp = array_flip(self::$CHANNEL_TYPE);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getIsTrackAttr($value)
    {
        return isset(self::$IS_TRACK[$value]) ? self::$IS_TRACK[$value] : $value;
    }

    protected function setIsTrackAttr($value)
    {
        $temp = array_flip(self::$IS_TRACK);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    public function carrier()
    {
        return $this->belongsTo(ChannelCarrier::class, 'carrier_id', 'carrier_id');
    }

    /**
     * 通过承运商ID获取渠道
     * @param int|int[] $carrierId 承运商 id
     * @return array|PDOStatement|string|Collection
     * @date 2020/09/04
     * @author longli
     */
    public static function getByCarrierId($carrierId)
    {
        return Channel::where([
            ['carrier_id', 'in', $carrierId],
            ['status', '=', Channel::STATUS_Y]
        ])->select();
    }
}
