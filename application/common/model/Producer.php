<?php

namespace app\common\model;


class Producer extends BaseModel
{
    protected $pk = 'producer_id';


    /**
     * 线上
     */
    const LINE_ON = 1;

    /**
     * 线下
     */
    const LINE_OFF = 0;

    /**
     * 代工厂
     */
    const LINE_AGENT = 2;

    /**
     * 中间商
     */
    const LINE_MIDD = 3;

    public static $LINE_TYPE = [
        self::LINE_ON => '线上',
        self::LINE_OFF => '线下',
        self::LINE_AGENT => '代工厂',
        self::LINE_MIDD => '中间商',
    ];

    /**
     * 启用
     */
    const STATUS_Y = 1;

    /**
     * 关闭
     */
    const STATUS_N = 0;

    public static $STATUS = [
        self::STATUS_Y => '启用',
        self::STATUS_N => '关闭'
    ];

    //
    /**
     * 银行转账
     */
    const PAY_TYPE_BANK = 1;

    /**
     * 支付宝
     */
    const PAY_TYPE_ALIPAY = 2;

    /**
     * 现金
     */
    const PAY_TYPE_CASH = 3;

    /**
     * 预付款
     */
    const PAY_TYPE_BEFORE = 4;

    /**
     * 账期
     */
    const PAY_TYPE_CREDIT = 5;

    public static $PAY_TYPE = [
        self::PAY_TYPE_BANK     => '银行转账',
        self::PAY_TYPE_ALIPAY   => '支付宝',
        self::PAY_TYPE_CASH     => '现金',
        self::PAY_TYPE_BEFORE   => '预付款',
        self::PAY_TYPE_CREDIT   => '账期',
    ];

    public function productPurchase()
    {
        return $this->hasMany(ProductPurchase::class);
    }

    /**
     * 供应商名称是否存在
     * @param string $name 分类名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function hasByName($name)
    {
        return !!static::where(['name' => $name])->count();
    }

    /**
     * 通过名称获取供应商
     * @param string $name 分类名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function getByName($name)
    {
        return static::where(['name' => $name])->find();
    }

    /**
     * 获取所有供应商
     * @return array|\PDOStatement|string|\think\Collection
     * @date 2020/09/15
     * @author longli
     */
    public static function getAll()
    {
        return static::where(['status' => self::STATUS_Y])->select();
    }
}
