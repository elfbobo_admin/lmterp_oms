<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/08/15
 * Time: 21:00
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


class ProductPurchase extends BaseModel
{
    protected $pk = 'pp_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 启用
     */
    const STATUS_Y = 1;

    /**
     * 关闭
     */
    const STATUS_N = 0;

    public static $STATUS = [
        self::STATUS_Y => '启用',
        self::STATUS_N => '关闭'
    ];

    protected function setStatusAttr($value)
    {
        $temp = array_flip(self::$STATUS);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS[$value]) ? self::$STATUS[$value] : $value;
    }

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    public function getIsDefaultAttr($value)
    {
        return isset(static::$IS_STATUS[$value]) ? static::$IS_STATUS[$value] : $value;
    }

    public function setIsDefaultAttr($value)
    {
        $temp = array_flip(static::$IS_STATUS);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    public function getTypeAttr($value)
    {
        return isset(Producer::$LINE_TYPE[$value]) ? Producer::$LINE_TYPE[$value] : $value;
    }

    public function setTypeAttr($value)
    {
        $temp = array_flip(Producer::$LINE_TYPE);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function producer()
    {
        return $this->belongsTo(Producer::class);
    }
}