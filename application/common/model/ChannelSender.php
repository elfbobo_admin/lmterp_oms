<?php

namespace app\common\model;

class ChannelSender extends BaseModel
{
    protected $pk = 'sender_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];


    /**
     * 默认
     */
    const DEFAULT_Y = 1;

    /**
     * 常规
     */
    const DEFAULT_N = 0;

    public static $IS_DEFAULT = [
        self::DEFAULT_Y => '默认',
        self::DEFAULT_N => '常规',
    ];

    /**
     * 可用
     */
    const STATUS_Y = 1;

    /**
     * 禁用
     */
    const STATUS_N = 0;

    public static $STATUS = [
        self::STATUS_Y => '可用',
        self::STATUS_N => '禁用',
    ];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }
}
