<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/6/19
 * Time: 13:02
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


/**
 * 用户模型
 * Class User
 * @package app\common\model
 */
class User extends BaseModel
{

    protected $readonly = ['uuid', 'register_time', 'mobile'];

    const STATUS_FORBIDDEN = 0;  //状态：禁用
    const STATUS_ACTIVE    = 1;   //状态:正常
    const STATUS_DELETE    = 2;   //状态:删除

}