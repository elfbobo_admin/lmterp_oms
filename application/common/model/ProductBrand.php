<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/08
 * Time: 13:52
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ProductBrand extends BaseModel
{
    protected $pk = 'brand_id';

    /**
     * 启用
     */
    const STATUS_Y = 1;

    /**
     * 关闭
     */
    const STATUS_N = 0;

    public static $STATUS = [
        self::STATUS_Y => '启用',
        self::STATUS_N => '关闭'
    ];

    public function products()
    {
        return $this->hasMany(ProductBrand::class, 'brand_id', 'brand_id');

    }

    protected function setStatusAttr($value)
    {
        $temp = array_flip(self::$STATUS);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS[$value]) ? self::$STATUS[$value] : $value;
    }

    /**
     * 检查品牌是否存在
     * @param string $brand 品牌中文名或英文名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function hasBrand($brand)
    {
        return !!static::whereOr('name_ch', $brand)
                ->whereOr('name_en', $brand)
                ->count();
    }

    /**
     * 获取名称获取品牌
     * @param string $brand 品牌名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function getByName($brand)
    {
        return static::whereOr('name_ch', $brand)
            ->whereOr('name_en', $brand)
            ->find();
    }

    /**
     * 获取所有品牌
     * @return array|\PDOStatement|string|\think\Collection
     * @date 2020/09/15
     * @author longli
     */
    public static function getAll()
    {
        return static::where(['status' => self::STATUS_Y])->order("sort desc, brand_id desc")->select();
    }

    /**
     * 更新品牌信息
     * @param int|int[] $ids 规格 id
     * @param array $data 规格信息
     * @return bool
     * @date 2020/09/15
     * @author longli
     */
    public static function editByBrandId($ids, $data)
    {
        try
        {
            foreach(static::where("brand_id", "in", $ids)->select() as $spec)
            {
                $spec->save($data);
            }
            return true;
        }catch(\Exception $e)
        {
            return false;
        }
    }
}
