<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/20
 * Time: 17:17
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class Exchange extends BaseModel
{
    protected $pk = 'exchange_id';

    public $autoWriteTimestamp = 'datetime';

    protected $createTime = false;
}
