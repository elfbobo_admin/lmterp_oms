<?php
/**
 * Created by Administrator
 * User: longli
 * Date: 2020/08/17
 * Time: 20:31
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


class OrdersTemp extends BaseModel
{
    protected $pk = 'temp_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['order_info'];

    // 把 json 返回格式转换为数组
    protected $jsonAssoc = true;

    /**
     * 订单已同步
     * @var int
     */
    const SYNC_ORDER_Y = 1;

    /**
     * 订单未同步
     * @var int
     */
    const SYNC_ORDER_N = 0;

    /**
     * 订单是否已在表中
     * @param string $orderNo 平台订单号
     * @param int $accountId 账号
     * @param string $site 站点，默认只检查订单号
     * @return bool
     * @date 2020/04/15
     * @author longli
     */
    public static function hasOrder($orderNo, $accountId = 0, $site = '')
    {
        $where = ['order_no' => $orderNo];
        if(!empty($accountId)) $where['account_id'] = $accountId;
        if(!empty($site)) $where['site'] = $site;
        return !!static::where($where)->count();
    }
}