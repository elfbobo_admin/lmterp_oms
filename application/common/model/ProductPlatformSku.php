<?php
/**
 * Created by Administrator
 * User: longli
 * Date: 2020/08/12
 * Time: 18:17
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


class   ProductPlatformSku extends BaseModel
{
    protected $pk = 'pid';

    protected $autoWriteTimestamp = 'datetime';

    protected $updateTime = false;

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by'];

    /**
     * 启用
     */
    const STATUS_Y = 1;

    /**
     * 关闭
     */
    const STATUS_N = 0;

    public static $STATUS = [
        self::STATUS_Y => '启用',
        self::STATUS_N => '关闭'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function store()
    {
        return $this->belongsTo(ProductStore::class, 'store_id', 'store_id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    protected function setStatusAttr($value)
    {
        $temp = array_flip(self::$STATUS);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS[$value]) ? self::$STATUS[$value] : $value;
    }

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    public static function getByPlatformSku($platformSku)
    {
        return static::get(['platform_sku' => $platformSku]);
    }

    /**
     * 平台 sku 是否存在
     * @param string $psku 平台 sku
     * @return bool
     * @date 2020/09/15
     * @author longli
     */
    public static function hasPlatformSku($psku)
    {
        return !!static::where(["platform_sku" => $psku])->count();
    }

    /**
     * 账号对应的变体是否存在
     * @param int $accountId 账号id
     * @param int $storeId 变体id
     * @return bool
     * @date 2020/09/15
     * @author longli
     */
    public static function hasByStoreAccount($accountId, $storeId)
    {
        return !!static::where(["store_id" => $storeId, "account_id" => $accountId])->count();
    }
}