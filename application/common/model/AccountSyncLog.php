<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/04/15
 * Time: 18:16
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

/**
 * 账号同步日志模型
 * Class AccountSyncLog
 * @package app\common\model
 */
class AccountSyncLog extends BaseModel
{
    /**
     * 添加同步日志
     * @param int $accountId 账号id
     * @param int $startTime 开始同步时间
     * @param int $endTime 同步完成时间
     * @param string $params 同步参数
     * @param string $msg 提示信息
     * @return AccountSyncLog
     * @date 2020/04/15
     * @author longli
     */
    public static function addLog($accountId, $startTime, $endTime, $params = '', $msg = '')
    {
        if(is_array($params)) $params = json_encode($params);
        if(is_array($msg)) $msg = json_encode($msg);
        return static::create([
            'account_id' => $accountId,
            'sync_start_time' => $startTime,
            'sync_finish_time' => $endTime,
            'params' => $params,
            'error_msg' => $msg,
        ]);
    }
}