<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/04/14
 * Time: 11:34
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use think\db\Query;
use think\model\relation\BelongsTo;

/**
 * 订单详情模型
 * Class OrderInfo
 * @package app\common\model
 */
class OrdersDetail extends BaseModel
{
    protected $pk = 'detail_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 未捆绑商品
     * @var int
     */
    const BUNDLE_N = 0;

    /**
     * 有捆绑商品
     * @var int
     */
    const BUNDLE_Y = 1;

    public static $BUNDLE_STATUS = [
        self::BUNDLE_N => '未捆绑',
        self::BUNDLE_Y => '有捆绑',
    ];

    /**
     * 未取消
     * @var int
     */
    const CANCEL_N = 0;

    /**
     * 已取消
     * @var int
     */
    const CANCEL_Y = 1;

    public static $CANCEL_STATUS = [
        self::BUNDLE_N => '未取消',
        self::BUNDLE_Y => '已取消',
    ];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function setUpdateByAttr()
    {
        if($this->isCli) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    protected function getIsBundleAttr($value)
    {
        return isset(self::$BUNDLE_STATUS[$value])
            ? self::$BUNDLE_STATUS[$value]
            : $value;
    }

    protected function getIsCancelAttr($value)
    {
        return isset(self::$CANCEL_STATUS[$value])
            ? self::$CANCEL_STATUS[$value]
            : $value;
    }

    protected function setIsBundleAttr($value)
    {
        $temp = array_flip(self::$BUNDLE_STATUS);
        return isset($temp[$value])
            ? $temp[$value]
            : $value;
    }

    protected function setIsCancelAttr($value)
    {
        $temp = array_flip(self::$CANCEL_STATUS);
        return isset($temp[$value])
            ? $temp[$value]
            : $value;
    }

    /**
     * 主订单
     * @return Query|BelongsTo
     * @date 2020/04/14
     * @author longli
     */
    public function order()
    {
        return $this->belongsTo(Orders::class);
    }

    public function productStore()
    {
        return $this->belongsTo(ProductStore::class, 'store_id', 'store_id');
    }
}