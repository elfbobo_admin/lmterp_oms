<?php


namespace app\common\model;

class SysTrash extends BaseModel
{
    protected $pk = 'trash_id';

    protected $insert = ['create_by'];

    protected function setCreateByAttr()
    {
        if($this->isCli) return 0;
        $session = session('lmterp');
        return $session ? $session->id : 0;
    }

    /**
     * 关联后台用户
     * @return \think\model\relation\BelongsTo
     * @date 2020/09/18
     * @author longli
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class, "create_by", "id");
    }
}