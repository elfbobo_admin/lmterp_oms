<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/09/07
 * Time: 20:15
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ProductUnit extends BaseModel
{
    protected $pk = 'unit_id';

    /**
     * 启用
     */
    const STATUS_Y = 1;

    /**
     * 关闭
     */
    const STATUS_N = 0;

    public static $STATUS = [
        self::STATUS_Y => '启用',
        self::STATUS_N => '关闭'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'unit_id', 'unit_id');
    }

    protected function setStatusAttr($value)
    {
        $temp = array_flip(self::$STATUS);
        return isset($temp[$value]) ? $temp[$value] : $value;
    }

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS[$value]) ? self::$STATUS[$value] : $value;
    }

    /**
     * 检查计量单位是否存在
     * @param string $unit 品牌中文名或英文名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function hasUnit($unit)
    {
        return !!static::whereOr('name_ch', $unit)
            ->whereOr('name_en', $unit)
            ->count();
    }

    /**
     * 通过名称获取计量单位
     * @param string $unit 品牌中文名或英文名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function getByName($unit)
    {
        return static::whereOr('name_ch', $unit)
            ->whereOr('name_en', $unit)
            ->find();
    }

    /**
     * 更新计量单位信息
     * @param int|int[] $ids 规格 id
     * @param array $data 规格信息
     * @return bool
     * @date 2020/09/14
     * @author longli
     */
    public static function editByUnitId($ids, $data)
    {
        try
        {
            foreach(static::where("unit_id", "in", $ids)->select() as $unit)
            {
                $unit->save($data);
            }
            return true;
        }catch(\Exception $e)
        {
            return false;
        }
    }

    /**
     * 获取所有计量单位
     * @return array|\PDOStatement|string|\think\Collection
     * @date 2020/09/15
     * @author longli
     */
    public static function getAll()
    {
        return static::where(['status' => self::STATUS_Y])->order("sort desc, unit_id desc")->select();
    }
}
