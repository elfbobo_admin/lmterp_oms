layui.define(['jquery', 'form', 'tree', 'layer', 'util'], function(exports) {
	var form = layui.form,
		$ = layui.jquery,
		tree = layui.tree,
		util = layui.util,
		index = 100;
	var obj = {
		ajax: function({
			elem: elem,
			url: ajaxUrl,
			type: type,
			dataType: dataType,
			data: data,
			placeholder:placeholder,
			callback: callback
		}) {
			//请求数据 创建组织树
			$('#' + elem).css('display', 'none');
			var dlHtml = '';
			dlHtml +=
				'<div class="layui-form-select selectGroupTree"><input id="" type="text" placeholder="'+placeholder+'" value="" readonly="readonly" class="layui-input layui-unselect "><i class="layui-edge"></i><dl  class="layui-anim layui-anim-upbit layui-group-dl"><dd><div id="'+elem+'_1'+'"></div></dd></dl></div>';
			$('#' + elem).parent('.layui-input-block').append(dlHtml);
			//请求数据 
			$.ajax({
				url: ajaxUrl,
				type: type,
				dataType: dataType,
				data: data,
				success: function(response) {
					if(response.code != 0) return;
					tree.render({
						elem: '#'+elem+'_1',
						data: response.data,
						onlyIconControl: true,
						click: function(node) {
							var data = node.data; //获取当前点击的节点数据
							$('#' + elem).parent().find('input').val(data.title);
							$('#' + elem).val(data.id);
							if(callback && typeof callback === "function")callback(node);
						}
					});
				}
			});
			setTimeout(function(){
				var $elem = $('#' + elem)
					, oval = $elem.val()
					, stv = $.trim($elem.val()) ? $elem.data("cate-name") : '--选择分类--';
				$elem.parent().find('input').val(stv);
				$elem.val(oval);
			}, 100);
			//点击弹出
			$('#' + elem).parent().find(".selectGroupTree").on("click", function(e) {
				var _this = $(this);
				_this.toggleClass("layui-form-selected");
				layui.stope(e);
			}).on("click", "dl i", function(e) {
				layui.stope(e);
			});
		}
	};


	//输出
	exports('selectGroupTree', obj);
});
