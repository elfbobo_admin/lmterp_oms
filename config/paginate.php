<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/2/7
 * Time: 13:08
 * @link http://www.lmterp.cn
 */
//分页配置
return [
    'type'      => 'app\\common\library\\LayPage',
    'var_page'  => 'page',
    'list_rows' => 100
];