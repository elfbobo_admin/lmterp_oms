<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/2/5
 * Time: 11:25
 * @link http://www.lmterp.cn
 */
return [
    'seKey'    => env('APP_KEY', 'think'),
    'length'   => 4,
    'fontSize' => 32
];