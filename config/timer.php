<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2020/8/15
 * Time: 下午2:14
 * 秒 分 时 日 月 星期几
 * crontab 格式 * *  *  *  * *    => "类"
 * *中间一个空格
 * 系统定时任务需要在swoole.php中开启
 * 自定义定时器不受其影响
 */

return [
    '* * * * * *' => app\swoole\timer\OneSecondTimer::class, // 每秒执行
    '0 * * * * *' => app\swoole\timer\OneMinuteTimer::class, // 每分钟执行
    '0 */10 * * * *' => app\swoole\timer\TenMinuteTimer::class, // 每10分钟执行
    '0 0 */1 * * *' => app\swoole\timer\OneHourTimer::class, // 每小时执行
    '0 0 0 * * *' => app\swoole\timer\MidnightTimer::class, // 每天凌晨12点执行
];
